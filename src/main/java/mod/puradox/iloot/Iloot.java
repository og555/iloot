package mod.puradox.iloot;

import net.minecraft.client.Minecraft;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.Mod;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

@Mod(
        modid = Iloot.MOD_ID,
        name = Iloot.MOD_NAME,
        version = Iloot.VERSION,
        acceptableRemoteVersions = "*"
)
public class Iloot {

    public static final String MOD_ID = "iloot";
    public static final String MOD_NAME = "Iloot";
    public static final String VERSION = "1.0.1";

    @Mod.Instance(MOD_ID)
    public static Iloot INSTANCE;

    public static File lootDir;

    public static File chestLootDir;
    public static File entityLootDir;
    public static File gameLootDir;

    @Mod.EventHandler
    public void preinit(FMLPreInitializationEvent event) throws IOException {
        if(event.getSide().isServer()) {
            lootDir = new File("./iloot_tables/");
        } else {
            lootDir = new File(Minecraft.getMinecraft().gameDir + "/iloot_tables/");
        }

        chestLootDir = new File(lootDir + "/chests/");
        entityLootDir = new File(lootDir + "/entities/");
        gameLootDir = new File(lootDir + "/gameplay/");

        if(!lootDir.exists()) {Files.createDirectory(Paths.get(String.valueOf(lootDir)));}

        if(!chestLootDir.exists()) {Files.createDirectory(Paths.get(String.valueOf(chestLootDir)));}
        if(!entityLootDir.exists()) {Files.createDirectory(Paths.get(String.valueOf(entityLootDir)));}
        if(!gameLootDir.exists()) {Files.createDirectory(Paths.get(String.valueOf(gameLootDir)));}

        MinecraftForge.EVENT_BUS.register(LootGen.class);
    }
}
